use relm::{connect, connect_stream};
use relm_derive::Msg;


const SCOLOR: &str = "red";
const RCOLOR: &str = "orange";
const ECOLOR: &str = "green";
const FCOLOR: &str = "black";

use std::str::FromStr;

use gtk::{
    Entry,
    EntryExt,
    ContainerExt,
    Inhibit,
    Label,
    LabelExt,
    WidgetExt,
    Window,
    GtkWindowExt,
    WindowType,
    GridExt,
};
use relm::{Relm, Update, Widget, WidgetTest};

struct Model {
    values: Values,
}

#[derive(Msg)]
enum Msg {
    Quit,
    F64,
    F64x,
    F32,
    F32x,
    P32E2x,
    P32E2i,
    P16E1x,
    P16E1i,
    P8E0x,
    P8E0i,
    I64,
    I32,
}

#[derive(Clone)]
struct Widgets {
    e_f64: Entry,
    e_f64x: Entry,
    e_f32: Entry,
    e_f32x: Entry,
    e_p32e2x: Entry,
    e_p32e2i: Entry,
    l_p32e2: Label,
    e_p16e1x: Entry,
    e_p16e1i: Entry,
    l_p16e1: Label,
    e_p8e0x: Entry,
    e_p8e0i: Entry,
    l_p8e0: Label,
    e_i64: Entry,
    e_i32: Entry,
    window: Window,
}

struct Win {
    model: Model,
    widgets: Widgets,
}

impl Win {
    fn update_values(&mut self) {
        let vals = &self.model.values;
        self.widgets.e_f64.set_text(&vals.e_f64.to_string());
        self.widgets.e_f64x.set_text(&Self::format_hex64(vals.e_f64x));
        self.widgets.e_f32.set_text(&vals.e_f32.to_string());
        self.widgets.e_f32x.set_text(&Self::format_hex32(vals.e_f32x));
        self.widgets.e_p32e2x.set_text(&Self::format_hex32(vals.e_p32e2x));
        self.widgets.e_p32e2i.set_text(&vals.e_p32e2i.to_string());
        self.widgets.l_p32e2.set_label(&p32e2_bits(P32E2::from_bits(vals.e_p32e2x)));
        self.widgets.e_p16e1x.set_text(&format!("0x_{:04x}", vals.e_p16e1x));
        self.widgets.e_p16e1i.set_text(&vals.e_p16e1i.to_string());
        self.widgets.l_p16e1.set_label(&p16e1_bits(P16E1::from_bits(vals.e_p16e1x)));
        self.widgets.e_p8e0x.set_text(&format!("0x_{:02x}", vals.e_p8e0x));
        self.widgets.e_p8e0i.set_text(&vals.e_p8e0i.to_string());
        self.widgets.l_p8e0.set_label(&p8e0_bits(P8E0::from_bits(vals.e_p8e0x)));
        self.widgets.e_i64.set_text(&vals.e_i64.to_string());
        self.widgets.e_i32.set_text(&vals.e_i32.to_string());
    }
    fn format_hex64(val: u64) -> String {
        let s = format!("{:016x}", val);
        let (s12, s34) = s.split_at(8);
        let (s1, s2) = s12.split_at(4);
        let (s3, s4) = s34.split_at(4);
        format!("0x_{}_{}_{}_{}", s1, s2, s3, s4)
    }
    fn format_hex32(val: u32) -> String {
        let s = format!("{:08x}", val);
        let (s1, s2) = s.split_at(4);
        format!("0x_{}_{}", s1, s2)
    }
}

impl Update for Win {
    // Specify the model used for this widget.
    type Model = Model;
    // Specify the model parameter used to init the model.
    type ModelParam = ();
    // Specify the type of the messages sent to the update function.
    type Msg = Msg;

    fn model(_: &Relm<Self>, _: ()) -> Model {
        Model {
            values : Values::new(),
        }
    }

    fn update(&mut self, event: Msg) {

        match event {
            Msg::F64 => {
                if let Some(val) = f64::from_str(&self.widgets.e_f64.get_text().unwrap()).ok() {
                    self.model.values = Values::from_f64(val);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::F64x => {
                if let Some(val) = u64::from_str_radix(&self.widgets.e_f64x.get_text().unwrap().replace("0x", "").replace("_", ""), 16).ok() {
                    self.model.values = Values::from_f64(f64::from_bits(val));
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::F32 => {
                if let Some(val) = f32::from_str(&self.widgets.e_f32.get_text().unwrap()).ok() {
                    self.model.values = Values::from_f32(val);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::I64 => {
                if let Some(val) = i64::from_str(&self.widgets.e_i64.get_text().unwrap()).ok() {
                    self.model.values = Values::from_i64(val);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::I32 => {
                if let Some(val) = i32::from_str(&self.widgets.e_i32.get_text().unwrap()).ok() {
                    self.model.values = Values::from_i32(val);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::P32E2i => {
                if let Some(val) = i32::from_str(&self.widgets.e_p32e2i.get_text().unwrap()).ok() {
                    self.model.values = Values::from_p32e2_x(val as u32);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::P16E1i => {
                if let Some(val) = i16::from_str(&self.widgets.e_p16e1i.get_text().unwrap()).ok() {
                    self.model.values = Values::from_p16e1_x(val as u16);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::P8E0i => {
                if let Some(val) = i8::from_str(&self.widgets.e_p8e0i.get_text().unwrap()).ok() {
                    self.model.values = Values::from_p8e0_x(val as u8);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::F32x => {
                if let Some(val) = u32::from_str_radix(&self.widgets.e_f32x.get_text().unwrap().replace("0x", "").replace("_", ""), 16).ok() {
                    self.model.values = Values::from_f32(f32::from_bits(val));
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            
            Msg::P32E2x => {
                if let Some(val) = u32::from_str_radix(&self.widgets.e_p32e2x.get_text().unwrap().replace("0x", "").replace("_", ""), 16).ok() {
                    self.model.values = Values::from_p32e2_x(val);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            
            Msg::P16E1x => {
                if let Some(val) = u16::from_str_radix(&self.widgets.e_p16e1x.get_text().unwrap().replace("0x", "").replace("_", ""), 16).ok() {
                    self.model.values = Values::from_p16e1_x(val);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            
            Msg::P8E0x => {
                if let Some(val) = u8::from_str_radix(&self.widgets.e_p8e0x.get_text().unwrap().replace("0x", "").replace("_", ""), 16).ok() {
                    self.model.values = Values::from_p8e0_x(val);
                    self.update_values();
                } else {
                    println!("Wrong number!");
                }
            }
            Msg::Quit => gtk::main_quit(),
        }
    }
}

impl Widget for Win {
    // Specify the type of the root widget.
    type Root = Window;

    // Return the root widget.
    fn root(&self) -> Self::Root {
        self.widgets.window.clone()
    }

    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        // Create the view using the normal GTK+ method calls.
        let separator = gtk::Separator::new(gtk::Orientation::Vertical);
        separator.set_size_request(10, -1);

        let grid = gtk::Grid::new();
        let e_f64 = Entry::new();
        let e_f64x = Entry::new();
        let e_f32 = Entry::new();
        let e_f32x = Entry::new();
        let e_p32e2x = Entry::new();//SpinButton::new_with_range(0., P32E2::NAN.to_bits() as f64, 1.);
        let e_p32e2i = Entry::new();
        let l_p32e2 = Label::new("");
        l_p32e2.set_use_markup(true);
        let e_p16e1x = Entry::new();
        let e_p16e1i = Entry::new();
        let l_p16e1 = Label::new("");
        l_p16e1.set_use_markup(true);
        let e_p8e0x = Entry::new();
        let e_p8e0i = Entry::new();
        let l_p8e0 = Label::new("");
        l_p8e0.set_use_markup(true);
        let e_i64 = Entry::new();
        let e_i32 = Entry::new();
        
        grid.attach(&Label::new("f64: "), 0, 0, 1, 1);
        grid.attach(&e_f64, 1, 0, 1, 1);
        grid.attach(&Label::new("f64 (x): "), 3, 0, 1, 1);
        grid.attach(&e_f64x, 4, 0, 1, 1);
        
        grid.attach(&Label::new("f32: "), 0, 1, 1, 1);
        grid.attach(&e_f32, 1, 1, 1, 1);
        grid.attach(&Label::new("f32 (x): "), 3, 1, 1, 1);
        grid.attach(&e_f32x, 4, 1, 1, 1);
        
        grid.attach(&Label::new("P32E2 (i): "), 0, 2, 1, 1);
        grid.attach(&e_p32e2i, 1, 2, 1, 1);
        grid.attach(&Label::new("P32E2 (x): "), 3, 2, 1, 1);
        grid.attach(&e_p32e2x, 4, 2, 1, 1);
        
        grid.attach(&l_p32e2, 0, 3, 5, 1);

        grid.attach(&Label::new("P16E1 (i): "), 0, 4, 1, 1);
        grid.attach(&e_p16e1i, 1, 4, 1, 1);
        grid.attach(&Label::new("P16E1 (x): "), 3, 4, 1, 1);
        grid.attach(&e_p16e1x, 4, 4, 1, 1);

        grid.attach(&l_p16e1, 0, 5, 5, 1);
        
        grid.attach(&Label::new("P8E0 (i): "), 0, 6, 1, 1);
        grid.attach(&e_p8e0i, 1, 6, 1, 1);
        grid.attach(&Label::new("P8E0 (x): "), 3, 6, 1, 1);
        grid.attach(&e_p8e0x, 4, 6, 1, 1);

        grid.attach(&l_p8e0, 0, 7, 5, 1);
        
        grid.attach(&Label::new("i64: "), 0, 8, 1, 1);
        grid.attach(&e_i64, 1, 8, 1, 1);
        grid.attach(&Label::new("i32: "), 3, 8, 1, 1);
        grid.attach(&e_i32, 4, 8, 1, 1);
        
        grid.attach(&separator, 2, 0, 1, 3);
        
        let window = Window::new(WindowType::Toplevel);

        window.set_title("Posit converter");
        
        window.add(&grid);

        window.show_all();
        
        connect!(relm, e_f64, connect_activate(_), Msg::F64);
        connect!(relm, e_f64x, connect_activate(_), Msg::F64x);
        connect!(relm, e_f32, connect_activate(_), Msg::F32);
        connect!(relm, e_f32x, connect_activate(_), Msg::F32x);
        connect!(relm, e_p32e2x, connect_activate(_), Msg::P32E2x);
        connect!(relm, e_p32e2i, connect_activate(_), Msg::P32E2i);
        connect!(relm, e_p16e1x, connect_activate(_), Msg::P16E1x);
        connect!(relm, e_p16e1i, connect_activate(_), Msg::P16E1i);
        connect!(relm, e_p8e0x, connect_activate(_), Msg::P8E0x);
        connect!(relm, e_p8e0i, connect_activate(_), Msg::P8E0i);
        connect!(relm, e_i64, connect_activate(_), Msg::I64);
        connect!(relm, e_i32, connect_activate(_), Msg::I32);
        
        connect!(relm, window, connect_delete_event(_, _), return (Some(Msg::Quit), Inhibit(false)));

        Win {
            model,
            widgets: Widgets {
                e_f64,
                e_f64x,
                e_f32,
                e_f32x,
                e_p32e2x,
                e_p32e2i,
                l_p32e2,
                e_p16e1x,
                e_p16e1i,
                l_p16e1,
                e_p8e0x,
                e_p8e0i,
                l_p8e0,
                e_i64,
                e_i32,
                window: window,
            },
        }
    }
}

impl WidgetTest for Win {
    type Widgets = Widgets;

    fn get_widgets(&self) -> Self::Widgets {
        self.widgets.clone()
    }
}

#[derive(Clone)]
struct Values {
    pub e_f64 : f64,
    pub e_f64x : u64,
    pub e_f32 : f32,
    pub e_f32x : u32,
    pub e_p32e2x : u32,
    pub e_p32e2i : i32,
    pub e_p16e1x : u16,
    pub e_p16e1i : i16,
    pub e_p8e0x : u8,
    pub e_p8e0i : i8,
    pub e_i64 : i64,
    pub e_i32 : i32,
}

impl Values {
    pub fn new() -> Self {
        Values {
            e_f64: 0.,
            e_f64x: 0,
            e_f32: 0.,
            e_f32x: 0,
            e_p32e2x: 0,
            e_p32e2i: 0,
            e_p16e1x: 0,
            e_p16e1i: 0,
            e_p8e0x: 0,
            e_p8e0i: 0,
            e_i64: 0,
            e_i32: 0,
        }
    }
    pub fn from_f64(val: f64) -> Self {
        let f64_v = val;
        let f32_v = val as f32;
        let i64_v = val as i64;
        let i32_v = val as i32;
        let p32_x = P32E2::from(val).to_bits();
        let p16_x = P16E1::from(val).to_bits();
        let p8_x = P8E0::from(val).to_bits();
        Self::from_vals(f64_v, f32_v, p32_x, p16_x, p8_x, i64_v, i32_v)
    }
    pub fn from_f32(val: f32) -> Self {
        let f32_v = val;
        let f64_v = val as f64;
        let i64_v = val as i64;
        let i32_v = val as i32;
        let p32_x = P32E2::from(val).to_bits();
        let p16_x = P16E1::from(val).to_bits();
        let p8_x = P8E0::from(val).to_bits();
        Self::from_vals(f64_v, f32_v, p32_x, p16_x, p8_x, i64_v, i32_v)
    }
    pub fn from_i64(val: i64) -> Self {
        let i64_v = val;
        let f32_v = val as f32;
        let f64_v = val as f64;
        let i32_v = val as i32;
        let p32_x = P32E2::from(val).to_bits();
        let p16_x = P16E1::from(val).to_bits();
        let p8_x = P8E0::from(val).to_bits();
        Self::from_vals(f64_v, f32_v, p32_x, p16_x, p8_x, i64_v, i32_v)
    }
    pub fn from_i32(val: i32) -> Self {
        let i32_v = val;
        let f32_v = val as f32;
        let f64_v = val as f64;
        let i64_v = val as i64;
        let p32_x = P32E2::from(val).to_bits();
        let p16_x = P16E1::from(val).to_bits();
        let p8_x = P8E0::from(val).to_bits();
        Self::from_vals(f64_v, f32_v, p32_x, p16_x, p8_x, i64_v, i32_v)
    }
    pub fn from_p32e2_x(val: u32) -> Self {
        let p32_x = val;
        let p32 = P32E2::from_bits(val);
        let f64_v = f64::from(p32);
        let f32_v = f32::from(p32);
        let p16_x = P16E1::from(p32).to_bits();
        let p8_x = P8E0::from(p32).to_bits();
        let i64_v = i64::from(p32);
        let i32_v = i32::from(p32);
        Self::from_vals(f64_v, f32_v, p32_x, p16_x, p8_x, i64_v, i32_v)
    }
    pub fn from_p16e1_x(val: u16) -> Self {
        let p16_x = val;
        let p16 = P16E1::from_bits(p16_x);
        let f64_v = f64::from(p16);
        let f32_v = f32::from(p16);
        let p32_x = P32E2::from(p16).to_bits();
        let p8_x = P8E0::from(p16).to_bits();
        let i64_v = i64::from(p16);
        let i32_v = i32::from(p16);
        Self::from_vals(f64_v, f32_v, p32_x, p16_x, p8_x, i64_v, i32_v)
    }
    pub fn from_p8e0_x(val: u8) -> Self {
        let p8_x = val;
        let p8 = P8E0::from_bits(p8_x);
        let f64_v = f64::from(p8);
        let f32_v = f32::from(p8);
        let p32_x = P32E2::from(p8).to_bits();
        let p16_x = P16E1::from(p8).to_bits();
        let i64_v = i64::from(p8);
        let i32_v = i32::from(p8);
        Self::from_vals(f64_v, f32_v, p32_x, p16_x, p8_x, i64_v, i32_v)
    }
    
    fn from_vals(f64_v: f64, f32_v: f32, p32_x: u32, p16_x: u16, p8_x: u8, i64_v: i64, i32_v: i32) -> Self {
        Values {
            e_f64 : f64_v,
            e_f64x: f64_v.to_bits(),
            e_f32 : f32_v,
            e_f32x: f32_v.to_bits(),
            e_p32e2x : p32_x,
            e_p32e2i : p32_x as i32,
            e_p16e1x : p16_x,
            e_p16e1i : p16_x as i16,
            e_p8e0x : p8_x,
            e_p8e0i : p8_x as i8,
            e_i64 : i64_v,
            e_i32 : i32_v,
        }
    }
}


use strfmt::{strfmt_map, Formatter};
use std::collections::HashMap;

fn p32e2_bits(p: P32E2) -> String {
    let rsize = p.regime_size();
    let esize = p.exponent_size();
    let fsize = p.fraction_size();
    let mut vars = HashMap::<String, i64>::new();
    vars.insert("sign".to_string(), p.sign() as i64);
    vars.insert("regime".to_string(), p.regime_bits() as i64);
    vars.insert("exp".to_string(), p.exponent_bits() as i64);
    vars.insert("fraction".to_string(), p.fraction_bits() as i64);
    let regime_fmt = format!(
        r#"<spancolor="{scolor}">{{sign: 1b}}</span><spancolor="{rcolor}">{{regime: {rsize}b}}</span>"#,
        rsize=rsize,
        scolor=SCOLOR,
        rcolor=RCOLOR,
    );
    let exp_fmt = format!(
        r#"<spancolor="{ecolor}">{{exp: {esize}b}}</span>"#,
        esize=esize,
        ecolor=ECOLOR,
    );
    let fraction_fmt = format!(
        r#"<spancolor="{fcolor}">{{fraction: {fsize}b}}</span>"#,
        fsize=fsize,
        fcolor=FCOLOR,
    );
    let f = |mut fmt: Formatter| {
            fmt.i64(*vars.get(fmt.key).unwrap())
    };
    
    let mut fmt = strfmt_map(&regime_fmt, &f).unwrap();
    if esize > 0 {
        fmt += &strfmt_map(&exp_fmt, &f).unwrap();
    }
    if fsize > 0 {
        fmt += &strfmt_map(&fraction_fmt, &f).unwrap();
    }

    fmt.replace("0", "◻").replace("1", "◼").replace(" ", "◻").replace("spancolor", "span color")
}

fn p16e1_bits(p: P16E1) -> String {
    let rsize = p.regime_size();
    let esize = p.exponent_size();
    let fsize = p.fraction_size();
    let mut vars = HashMap::<String, i64>::new();
    vars.insert("sign".to_string(), p.sign() as i64);
    vars.insert("regime".to_string(), p.regime_bits() as i64);
    vars.insert("exp".to_string(), p.exponent_bits() as i64);
    vars.insert("fraction".to_string(), p.fraction_bits() as i64);
    let regime_fmt = format!(
        r#"<spancolor="{scolor}">{{sign: 1b}}</span><spancolor="{rcolor}">{{regime: {rsize}b}}</span>"#,
        rsize=rsize,
        scolor=SCOLOR,
        rcolor=RCOLOR,
    );
    let exp_fmt = format!(
        r#"<spancolor="{ecolor}">{{exp: {esize}b}}</span>"#,
        esize=esize,
        ecolor=ECOLOR,
    );
    let fraction_fmt = format!(
        r#"<spancolor="{fcolor}">{{fraction: {fsize}b}}</span>"#,
        fsize=fsize,
        fcolor=FCOLOR,
    );
    let f = |mut fmt: Formatter| {
            fmt.i64(*vars.get(fmt.key).unwrap())
    };
    
    let mut fmt = strfmt_map(&regime_fmt, &f).unwrap();
    if esize > 0 {
        fmt += &strfmt_map(&exp_fmt, &f).unwrap();
    }
    if fsize > 0 {
        fmt += &strfmt_map(&fraction_fmt, &f).unwrap();
    }

    fmt.replace("0", "◻").replace("1", "◼").replace(" ", "◻").replace("spancolor", "span color")
}

fn p8e0_bits(p: P8E0) -> String {
    let rsize = p.regime_size();
    let esize = p.exponent_size();
    let fsize = p.fraction_size();
    let mut vars = HashMap::<String, i64>::new();
    vars.insert("sign".to_string(), p.sign() as i64);
    vars.insert("regime".to_string(), p.regime_bits() as i64);
    vars.insert("exp".to_string(), p.exponent_bits() as i64);
    vars.insert("fraction".to_string(), p.fraction_bits() as i64);
    let regime_fmt = format!(
        r#"<spancolor="{scolor}">{{sign: 1b}}</span><spancolor="{rcolor}">{{regime: {rsize}b}}</span>"#,
        rsize=rsize,
        scolor=SCOLOR,
        rcolor=RCOLOR,
    );
    let exp_fmt = format!(
        r#"<spancolor="{ecolor}">{{exp: {esize}b}}</span>"#,
        esize=esize,
        ecolor=ECOLOR,
    );
    let fraction_fmt = format!(
        r#"<spancolor="{fcolor}">{{fraction: {fsize}b}}</span>"#,
        fsize=fsize,
        fcolor=FCOLOR,
    );
    let f = |mut fmt: Formatter| {
            fmt.i64(*vars.get(fmt.key).unwrap())
    };
    
    let mut fmt = strfmt_map(&regime_fmt, &f).unwrap();
    if esize > 0 {
        fmt += &strfmt_map(&exp_fmt, &f).unwrap();
    }
    if fsize > 0 {
        fmt += &strfmt_map(&fraction_fmt, &f).unwrap();
    }

    fmt.replace("0", "◻").replace("1", "◼").replace(" ", "◻").replace("spancolor", "span color")
}

extern crate softposit;
use softposit::{P32E2, P16E1, P8E0};

fn main() {
    Win::run(()).expect("Win::run failed");
}


trait BitsType {
    type Bits;
}

impl BitsType for P32E2 {
    type Bits = u32;
}
impl BitsType for P16E1 {
    type Bits = u16;
}
impl BitsType for P8E0 {
    type Bits = u8;
}

use core::cmp;

trait Size: BitsType {
    fn nbits() -> usize;

    /// Returns size, in bits, of the fraction component of `self`
    fn fraction_size(self) -> u8;
    
    /// Returns size, in bits, of the exponent component of `self`
    fn exponent_size(self) -> u8;
    
    /// Returns the size, in bits, of the regime component
    fn regime_size(self) -> u8;
    
    /// Returns the exponent component of `self`
    fn exponent(self) -> Self::Bits;
    
    /// Returns the fraction component of `self`
    fn fraction(self) -> Self::Bits;
    
    /// Returns the sign bit
    fn sign(self) -> Self::Bits;

    /// Returns the regime as an exponent
    fn regime(self) -> i8;

    fn fraction_bits(self) -> Self::Bits;
    fn exponent_bits(self) -> Self::Bits;
    fn regime_bits(self) -> Self::Bits;
    fn regime_mask(self) -> Self::Bits;
    fn exponent_mask(self) -> Self::Bits;
    fn fraction_mask(self) -> Self::Bits;
}
            
impl Size for P16E1 {
    fn nbits() -> usize {
        16
    }

    /// Returns size, in bits, of the fraction component of `self`
    fn fraction_size(self) -> u8 {
        Self::nbits() as u8 - 1 - self.regime_size() - self.exponent_size()
    }
    
    /// Returns size, in bits, of the exponent component of `self`
    fn exponent_size(self) -> u8 {
        cmp::min(Self::nbits() as u8 - 1 - self.regime_size(), Self::ES as u8)
    }
    
    /// Returns the size, in bits, of the regime component
    fn regime_size(self) -> u8 {
        // left aligned bits
        let bits = self.abs().to_bits() << (Self::SIZE - Self::nbits());

        // chop off the sign bit
        let bits = bits << 1;

        let lz = bits.leading_zeros() as u8;
        let lo = (!bits).leading_zeros() as u8;
        let rs = cmp::max(lz, lo) + 1;

        cmp::min(rs, Self::nbits() as u8 - 1)
    }
    
    /// Returns the exponent component of `self`
    fn exponent(self) -> Self::Bits {
        (self.abs().to_bits() >> self.fraction_size()) & self.exponent_mask()
    }
    
    /// Returns the fraction component of `self`
    fn fraction(self) -> Self::Bits {
        self.abs().to_bits() & self.fraction_mask()
    }
    
    /// Returns the sign bit
    fn sign(self) -> Self::Bits {
        (self.to_bits() & Self::SIGN_MASK) >> (Self::nbits() - 1)
    }

    /// Returns the regime as an exponent
    fn regime(self) -> i8 {
        // left aligned bits
        let bits = self.abs().to_bits() << (Self::SIZE - Self::nbits());

        // chop off the sign bit
        let bits = dbg!(bits << 1);

        let lz = bits.leading_zeros() as i8;
        dbg!(lz);
        let lo = (!bits).leading_zeros() as i8;
        dbg!(lo);

        if lz == 0 { lo - 1 } else { -lz }
    }

    fn fraction_bits(self) -> Self::Bits {
        self.to_bits() & self.fraction_mask()
    }

    fn exponent_bits(self) -> Self::Bits {
        (self.to_bits() >> self.fraction_size()) & self.exponent_mask()
    }

    fn regime_bits(self) -> Self::Bits {
        (self.to_bits() >> self.fraction_size() >> self.exponent_size()) & self.regime_mask()
    }

    fn regime_mask(self) -> Self::Bits {
        (1 << self.regime_size()) - 1
    }

    fn exponent_mask(self) -> Self::Bits {
        (1 << self.exponent_size()) - 1
    }

    fn fraction_mask(self) -> Self::Bits {
        (1 << self.fraction_size()) - 1
    }
}
impl Size for P8E0 {
    fn nbits() -> usize {
        8
    }

    /// Returns size, in bits, of the fraction component of `self`
    fn fraction_size(self) -> u8 {
        Self::nbits() as u8 - 1 - self.regime_size() - self.exponent_size()
    }
    
    /// Returns size, in bits, of the exponent component of `self`
    fn exponent_size(self) -> u8 {
        cmp::min(Self::nbits() as u8 - 1 - self.regime_size(), Self::ES as u8)
    }
    
    /// Returns the size, in bits, of the regime component
    fn regime_size(self) -> u8 {
        // left aligned bits
        let bits = self.abs().to_bits() << (Self::SIZE - Self::nbits());

        // chop off the sign bit
        let bits = bits << 1;

        let lz = bits.leading_zeros() as u8;
        let lo = (!bits).leading_zeros() as u8;
        let rs = cmp::max(lz, lo) + 1;

        cmp::min(rs, Self::nbits() as u8 - 1)
    }
    
    /// Returns the exponent component of `self`
    fn exponent(self) -> Self::Bits {
        (self.abs().to_bits() >> self.fraction_size()) & self.exponent_mask()
    }
    
    /// Returns the fraction component of `self`
    fn fraction(self) -> Self::Bits {
        self.abs().to_bits() & self.fraction_mask()
    }
    
    /// Returns the sign bit
    fn sign(self) -> Self::Bits {
        (self.to_bits() & Self::SIGN_MASK) >> (Self::nbits() - 1)
    }

    /// Returns the regime as an exponent
    fn regime(self) -> i8 {
        // left aligned bits
        let bits = self.abs().to_bits() << (Self::SIZE - Self::nbits());

        // chop off the sign bit
        let bits = dbg!(bits << 1);

        let lz = bits.leading_zeros() as i8;
        dbg!(lz);
        let lo = (!bits).leading_zeros() as i8;
        dbg!(lo);

        if lz == 0 { lo - 1 } else { -lz }
    }

    fn fraction_bits(self) -> Self::Bits {
        self.to_bits() & self.fraction_mask()
    }

    fn exponent_bits(self) -> Self::Bits {
        (self.to_bits() >> self.fraction_size()) & self.exponent_mask()
    }

    fn regime_bits(self) -> Self::Bits {
        (self.to_bits() >> self.fraction_size() >> self.exponent_size()) & self.regime_mask()
    }

    fn regime_mask(self) -> Self::Bits {
        (1 << self.regime_size()) - 1
    }

    fn exponent_mask(self) -> Self::Bits {
        (1 << self.exponent_size()) - 1
    }

    fn fraction_mask(self) -> Self::Bits {
        (1 << self.fraction_size()) - 1
    }
}
impl Size for P32E2 {
    fn nbits() -> usize {
        32
    }

    /// Returns size, in bits, of the fraction component of `self`
    fn fraction_size(self) -> u8 {
        Self::nbits() as u8 - 1 - self.regime_size() - self.exponent_size()
    }
    
    /// Returns size, in bits, of the exponent component of `self`
    fn exponent_size(self) -> u8 {
        cmp::min(Self::nbits() as u8 - 1 - self.regime_size(), Self::ES as u8)
    }
    
    /// Returns the size, in bits, of the regime component
    fn regime_size(self) -> u8 {
        // left aligned bits
        let bits = self.abs().to_bits() << (Self::SIZE - Self::nbits());

        // chop off the sign bit
        let bits = bits << 1;

        let lz = bits.leading_zeros() as u8;
        let lo = (!bits).leading_zeros() as u8;
        let rs = cmp::max(lz, lo) + 1;

        cmp::min(rs, Self::nbits() as u8 - 1)
    }
    
    /// Returns the exponent component of `self`
    fn exponent(self) -> Self::Bits {
        (self.abs().to_bits() >> self.fraction_size()) & self.exponent_mask()
    }
    
    /// Returns the fraction component of `self`
    fn fraction(self) -> Self::Bits {
        self.abs().to_bits() & self.fraction_mask()
    }
    
    /// Returns the sign bit
    fn sign(self) -> Self::Bits {
        (self.to_bits() & Self::SIGN_MASK) >> (Self::nbits() - 1)
    }

    /// Returns the regime as an exponent
    fn regime(self) -> i8 {
        // left aligned bits
        let bits = self.abs().to_bits() << (Self::SIZE - Self::nbits());

        // chop off the sign bit
        let bits = dbg!(bits << 1);

        let lz = bits.leading_zeros() as i8;
        dbg!(lz);
        let lo = (!bits).leading_zeros() as i8;
        dbg!(lo);

        if lz == 0 { lo - 1 } else { -lz }
    }

    fn fraction_bits(self) -> Self::Bits {
        self.to_bits() & self.fraction_mask()
    }

    fn exponent_bits(self) -> Self::Bits {
        (self.to_bits() >> self.fraction_size()) & self.exponent_mask()
    }

    fn regime_bits(self) -> Self::Bits {
        (self.to_bits() >> self.fraction_size() >> self.exponent_size()) & self.regime_mask()
    }

    fn regime_mask(self) -> Self::Bits {
        (1 << self.regime_size()) - 1
    }

    fn exponent_mask(self) -> Self::Bits {
        (1 << self.exponent_size()) - 1
    }

    fn fraction_mask(self) -> Self::Bits {
        (1 << self.fraction_size()) - 1
    }
}
